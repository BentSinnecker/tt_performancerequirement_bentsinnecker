﻿using UnityEngine;

namespace TTPP
{

    public class PLayerBehaviour : MonoBehaviour
    {
        public int health;
        public int maxHealth;
        public int healingAmount;

        public int damage;
        public float stamina;

        public string PlayerName = "Link";
        public bool isPlayerAlive = true;

        private void Awake()
        {
            InitHealth();
        }

        private void InitHealth()
        {
            health = maxHealth;
        }

        private void PlayerHealing(int healthAmount)
        {
            health = healthAmount;
        }

        public void AddSmallHealth()
        {
            health += healingAmount;
        }

        public void AddMediumHealth()
        {
            health += healingAmount * 2;
        }

        private void AddBigHealth()
        {
            health = maxHealth;
        }
    }

}